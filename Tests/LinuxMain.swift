import XCTest

import aerc_contacts_cliTests

var tests = [XCTestCaseEntry]()
tests += aerc_contacts_cliTests.allTests()
XCTMain(tests)
